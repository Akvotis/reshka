export function slider(selector) {
    const swiper = new Swiper(selector, {
        speed: 500,
        spaceBetween: 80,
        slidesPerView: 1,
        watchSlidesProgress: true,
        autoplay: {
            delay: 5000,
            disableOnInteraction: false
        },
        effect: 'fade',
        fadeEffect: {
          crossFade: true
        },
        navigation: {
            nextEl: ".swiper-button-next",
            prevEl: ".swiper-button-prev",
        },
        pagination: {
            el: ".swiper-pagination",
            type: "fraction",
            formatFractionCurrent: addZero,
            formatFractionTotal: addZero
        },
        on: {
            init: function () {
                limitTextHeight();
                replaceLink();
                progressBar(this.params.autoplay.delay);
            },
            slideChangeTransitionEnd: function() {
                if(window.screen.availWidth <= 1024) {
                    replaceLink();
                }
            },
            slideChange: function() {
                progressBar(swiper.params.autoplay.delay);
            }
        },
        breakpoints: {
            320: {
                spaceBetween: 20
            },
            768: {
                spaceBetween: 40
            },
            1024: {
                spaceBetween: 60
            }
        }
    });

    function addZero(num){
        return (num > 9) ? num : '0' + num;
    }

    function limitTextHeight() {
        const titles = document.querySelectorAll('.js-title');
        const desks = document.querySelectorAll('.js-desk');

        let sizes = {
            'title' :120,
            'desk' : 128
        };

        if(window.screen.availWidth < 1200) {
            sizes.title = 90;
            sizes.desk = 102;
        } else if(window.screen.availWidth < 1024) {
            sizes.title = 64;
            sizes.desk = 78;
        } else if(window.screen.availWidth < 768) {
            sizes.title = 44;
            sizes.desk = 36;
        } else if(window.screen.availWidth < 375) {
            sizes.title = 34;
            sizes.desk = 36;
        }

        titles.forEach((element, ind) => {
            if(element.offsetHeight > sizes.title) {
                desks[ind].style.cssText = `max-height: ${sizes.desk}px;`;
            } else if(desks[ind].offsetHeight > sizes.desk) {
                element.style.cssText = `max-height: ${sizes.title}px;`;
            }
        });
    }

    function replaceLink() {
        let willLink = document.querySelector('.js-link');
        let link = document.querySelector('.swiper-slide-active a');

        willLink.href = link.href;
    }

    function progressBar(time) {
        let start = 0;
        let seconds = Math.round(time/100);
        let progressElement = document.querySelector('.js-progress');

        let intervalId = setInterval(function() {
            if(start > 100) {
                clearInterval(intervalId)
            } else {
                progressElement.value = start;
            }
            start++;
        }, seconds);
    }
}